<?php

declare(strict_types = 1);

namespace App\Repository;

use App\Entity\Pizzeria;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * Class PizzeriaRepository
 * @package App\Repository
 */
class PizzeriaRepository extends ServiceEntityRepository
{
    /**
     * PizzeriaRepository constructor.
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Pizzeria::class);
    }

    /**
     * @param int $pizzeriaId
     * @return Pizzeria
     */
    public function findCartePizzeria($pizzeriaId): Pizzeria
    {

        //création du queryBuilder avec pi pour pizzeria
        $qb = $this->createQueryBuilder("pi");

        //éléments de la requête
        $qb
            ->addSelect(["piz", "quant", "ingr"])
            ->innerJoin("pi.pizzas", "piz")
            ->innerJoin("piz.quantiteIngredients", "quant")
            ->innerJoin("quant.ingredient", "ingr")
            ->where("pi.id = :idPizzeria")
            ->setParameter("idPizzeria", $pizzeriaId)
        ;

        //on execute la requête
        return $qb->getQuery()->getSingleResult();
    }
}
