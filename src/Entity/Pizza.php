<?php

declare(strict_types = 1);

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="pizza")
 * @ORM\Entity(repositoryClass="App\Repository\PizzaRepository")
 */
class Pizza
{
    /**
     * @var int
     * @ORM\Column(name="id_pizza", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="nom", type="string", length=255, unique=true)
     */
    private $nom;

    /**
     * @var Collection
     * liaison One-To-Many unidirectional with Join Table
     * Many Pizzas have Many Ingredients
     * @ORM\ManyToMany(targetEntity="App\Entity\IngredientPizza")
     * @ORM\JoinTable(name="ingredients_pizzas",
     *      joinColumns={@ORM\JoinColumn(name="pizza_id", referencedColumnName="id_pizza")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="ingredient_pizza_id", referencedColumnName="id_ingredient_pizza", unique=true)}
     *      )
     */
    private $quantiteIngredients;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->quantiteIngredients = new ArrayCollection();
    }

    /**
     * @return float
     */
    //méthode qui permet d'afficher le prix de fabrication de la pizza sélectionnée
    public function getCoutFabrication()
    {
        //initialisation de la variable qui stock le prix des ingrédients au fur et mesure que la boucle est parcourue
        $prixPizza = 0;

        //boucle qui est parcouru afin de calculer et stocker le coût de chaque ingredient par rapport à la pizza sélectionnée
        foreach($this->quantiteIngredients as $ingredientPizza){

            //on stock la quantité d'ingrédients grâce à la méthode getQuantite()
            $quantite = $ingredientPizza->getQuantite();

            //on convertit la quantité d'ingrédients de g en kg grâce à la méthode convertirGrammeEnKoli dans IngrédientPizza
            $kilo = IngredientPizza::convertirGrammeEnKilo($quantite);

            //on stock l'ingredient courant de la classe IngredientPizza grâce à la méthode getIngredient()
            $ingredrient = $ingredientPizza->getIngredient();

            //on obtient le cout de l'ingrédient courant
            $coutKilo = $ingredrient->getCout();

            //on additionne le prix total de la pizza avec le prix de l'ingrédient courant
            $prixPizza += $kilo*$coutKilo;
        }

        //on retourne le cout total de chaque ingrédients de la pizza
        return $prixPizza;
    }
    
    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Pizza
     */
    public function setId(int $id): Pizza
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getNom(): ?string
    {
        return $this->nom;
    }

    /**
     * @param string $nom
     * @return Pizza
     */
    public function setNom(string $nom): Pizza
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * @param IngredientPizza $quantiteIngredients
     * @return Pizza
     */
    public function addQuantiteIngredients(IngredientPizza $quantiteIngredients): Pizza
    {
        $this->quantiteIngredients[] = $quantiteIngredients;

        return $this;
    }

    /**
     * @param IngredientPizza $quantiteIngredients
     */
    public function removeQuantiteIngredient(IngredientPizza $quantiteIngredients): void
    {
        $this->quantiteIngredients->removeElement($quantiteIngredients);
    }

    /**
     * @return Collection
     */
    public function getQuantiteIngredients(): Collection
    {
        return $this->quantiteIngredients;
    }
}
